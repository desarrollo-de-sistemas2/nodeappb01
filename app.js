const express = require('express');
const pg = require('pg');
const app = express();

const pool = new pg.Pool({
    host: 'localhost',
    port: 5432,
    user: 'postgres',
    password: 'mamachu123',
    database: 'ymedranda'
});

pool.connect((err, client, release) => {
    if (err) {
        console.error("Error, no se puede conectar con la base de datos", err);
    } else {
        console.log("Se conectó con la base de datos ");
        console.log("Dirijase a localhost:3000")
    }
});

// Cambiando usuario de computador, commit incial cambiado por el propietario del equipo
app.get('/', (req, res) => {
    res.send("Servidor funcionando, se ha conectado con la base de datos");
    
});

app.get('/owner', (req, res) => {
    res.send("Yessenia Medranda");
    
});


app.listen(3000, () => {
    console.log("Server running on port 3000");
});